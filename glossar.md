# 📚 Glossar

## H

* **High Efficiency Image Compressor** \(HEIC\)     
  Apple Format zum platzsparenden Ablege großer Foto-Dateien.

## O

* **Original Video Animation** \(OVA\)     

  Videos die nur zu Verkaufszwecken produziert wurden und nicht im Fernsehen gezeigt wurden. Zum Beispiel um die Exklusivität von BlueRay-Boxen zu erhöhen.

