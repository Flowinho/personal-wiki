# 🏙 HEIC

**H**igh **E**fficiency **I**mage **C**ompression

- Apple Format um Fotos, Portraitmode-Fotos und Live-Pictures platzsparend abzulegen.
- Beste Software um HEIC in JPG zu konvertieren: https://imazing.com/de/heic
