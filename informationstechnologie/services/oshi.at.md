# oshi.at

{% hint style="danger"%}
Obwohl von oshi.at vermutlich keine Gefahr ausgeht ist von einer Verwendung innerhalb von Firmen-Netzwerken oder auf einem Arbeitsgerät dass einem Arbeitgeber gehört dringlichst abzuraten.
{% endhint %}

![](oshi.png)

- Uploadservice der das einfache Versenden von Dateien über das Terminal zulässt.
- Stark geschützt, potentiell allerdings merkwürdig, sensible Dateien **unbedingt verschlüsseln.**
- Unterstützt eine Anpassung der Parameter mit denen eine Datei gespeichert wird:
  - anderer Dateiname
  - eigenes Ablaufdatum
  - automatisches Löschen nach einem Download
- Kann potentiell selbst gehostet werden.
- Unterstützt Up und Download aus dem TOR-Netzwerk.

- https://oshi.at
- https://github.com/somenonymous/OshiUpload

## Auszug aus der Page "Drama section"  

> All the data is located in a 3rd world country where perception and definition of terms such as "unlawful" may differ from yours. Please use the form below to get our concern about some stored file, but don't expect much because we generally don't give a fuck what is uploaded over here and we are against any censorship on the Internet. The only subject we will react fast to is child pornography. Do not ask us to provide IP addresses or any other identifying information, we don't store it in order to protect your privacy... you're welcome. Be polite if you wish to comment your request.

[Oshi.at/abuse](https://oshi.at/abuse)

## Mit dem Terminal hochladen

```bash
curl -T /path/to/file https://oshi.at
curl -T /path/to/file https://oshi.at/customfilename # use a different filename
curl -T /path/to/file https://oshi.at/customfilename/15 # use a different filename and expire in 15 minutes
curl --upload-file /path/to/file https://oshi.at/customfilename/60 # expire in 60 minutes
curl -T /path/to/file https://oshi.at/customfilename/-1 # auto-destroy after first download
curl -T /path/to/file http://oshiatwowvdbshka.onion # upload using our Tor hidden service
```