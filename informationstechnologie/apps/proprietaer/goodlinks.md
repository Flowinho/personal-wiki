# Goodlinks

- Read-it-later-App.
- Synchronisiert über iCloud mit der macOS-Version.
- Einmalkauf spart Abo bei serverbasierten Diensten, zB. [Instapaper](https://instapaper.com).
- Sehr performante, nativ programmierte App in typischer iOS Design-Sprache.
- [MacStories Review](https://www.macstories.net/reviews/goodlinks-review-a-flexible-read-it-later-link-manager-packed-with-automation-options/)

{% embed url=https://apps.apple.com/de/app/goodlinks/id1474335294 %}