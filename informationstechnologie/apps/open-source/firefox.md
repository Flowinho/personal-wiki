---
description: >-
  Browser für iPhone / iPad mit Fokus auf Open-Source Technologie und
  Privatsphäre.
---

# Firefox

**Preis**: Kostenlos

* Guter Kompromiss zwischen Privatsphäre und Bedienbarkeit.
* Nicht ganz so stark konfigurierbar wie [SnowHaze](https://apps.apple.com/de/app/snowhaze/id1121026941) - dafür um einiges besser zu bedienen und zuverlässiger.
* Unterstützt die Einbindung von eigenen Suchmaschinen wie beispielsweise [Searx](https://searx.me/).
* Synchronisiert Tabs und Lesezeichen falls gewünscht mit der Desktop-Version.

{% embed url="https://apps.apple.com/de/app/internet-browser-firefox/id989804926" %}



