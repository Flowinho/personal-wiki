# 📱 Apps

![](../../.gitbook/assets/iVBORw0KGgoAAAANSUhEUgAABkAAAArwCAYAAAD8ZvemAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJ8WlUWHRYTUw6Y29tLmFkb2Jl-2.PNG)

Ich versuche die Apps auf meinem iPhone nach folgenden Kriterien auszuwählen:

* Open Source
* Ohne Abo
* Native Apps, keine Web-Applikationen
* Wenn möglich, ohne Account

