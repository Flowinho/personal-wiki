# Xcode 12 Fehlermeldung beim Debuggen: Cannot attach to process

## Das Problem 💢

**Szenario:** iOS 14, Xcode 12, macOS Catalina

Ein App-Projekt \(kein Cocoapods, kein Carthage\) ließ sich zwar im Simulator ausführen, auf dem echten Device gab es aber Probleme. Da die Umstände des Projektes erforderten dass die App auf einem verbundenen iPhone nicht nur gestartet sondern auch gedebugged werden kann, verhindert ein Fehler die Weiterentwicklung:

{% hint style="danger" %}
ERROR: Cannot attach to process . Make sure the device is connected...
{% endhint %}

## Lösung 😌

- Alle Entwicklungsprofile gegenprüfen ob es zu Kollisionen in der Device-Security kommt. 

## Hintergrund 📚

In meinem Fall wurde ein Enterprise Distribution Profile mit den falschen Identifieren eines Provisioning Profiles zusammengebracht. Dadurch konnte die App installiert,
aber nicht ausgeführt / gedebugged werden.
