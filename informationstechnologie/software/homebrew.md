# Homebrew

- Fehlender Package-Manager unter macOS.
- **Must-have**, auch für Laien gut nutzbar.

{% embed url="https://brew.sh/" %}

## Installation ⚙️

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

## Nutzung 👨🏻‍💻

- Binary installieren: `brew cask install <application-name>`
- CLI installieren: `brew install <cli-integration>`
- Ab und an den Homebrew Cache leeren: `rm -rf ~/Library/Caches/Homebrew`
