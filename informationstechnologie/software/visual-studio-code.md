# Visual Studio Code

![](../../.gitbook/assets/vs-code.png)

[Visual Studio Code](https://code.visualstudio.com/) ist mein favorisierter Editor für quasi jeden Anwendungsfall.

* Ich nutze VSCode für folgende Sprachen und / oder Anwendungsfälle:
  * LaTeX
  * Markdown \(diese Wiki\)
  * HTML / JavaScript
* Große Community die den Editor um viele Funktionen erweitert. 
* Im Vergleich zu beispielsweise [GitHub Atom](https://github.atom.io/) schneller und performanter.
* Verfübar auf nahezu allen Desktop-Betriebssystemen.

## Installation

```bash
brew cask install visual-studio-code
```

## Einrichtung

### Erweiterungen

- Bracket Pair Colorizer
- File Utlis
- GitLens
- JSON Pretty Printer
- LaTeX Workshop
- Markdown All in One
- Prettier
- Property List
- Swift Language
- Syntax Code Project Data

### Themes 👨🏻‍🎨

* Material Light
* Materical Icon Theme

### Packages 📦

* GitLens -- Git supercharged
* LaTeX Workshop

