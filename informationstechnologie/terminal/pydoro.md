# Pydoro

Terminal-Applikation um Pomodoros anzuwenden.

- Benötigt Python 3, macOS wird mit Python 3 ausgeliefert.
- [Github Link](https://github.com/JaDogg/pydoro)

## Installieren (macOS)

```bash
brew install python # Falls Python3 nicht installiert ist.
sudo pip install --upgrade pip
sudo pip install pydoro 
```

## Benutzung

