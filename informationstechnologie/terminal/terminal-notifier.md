# terminal-notifier

Eine kleine Erweiterung des macOS-Terminals um Notifications anzuzeigen.

## Installation

```bash
brew install terminal-notifier
```

## Nutzung

Zum Beispiel lässt sich ein Pomodoro daraus bauen.

```bash
sleep "1800" && terminal-notifier -message "POMODORO VORBEI"
```