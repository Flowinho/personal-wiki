# tar

## Schalter

- **c**     
  Compress - *In ein Archiv hinzufügen.*
- **v**     
  Verbose - *Der Vorgang wird auf dem Terminal dargestellt.*
- **f**     
  File - *Folgend der Dateiname des Archivs.*
- **x**     
  Expand - *Aus Archiv auspacken.*
- **z**     
  zip - *Dateigrößenreduktion anwenden.*
- **t**     
  tree - *Dateiliste im Archiv anzeigen.*

## Beispiele

Dateien in ein komprimiertes Tar-Archiv verschieben:

```bash
tar cvfz fileName.tar /path/to/files
```

Dateien aus Tar-Archiv holen:

```bash
tar xvfz fileName.tar
```

Den Inhalt eines komprimierten Tar-Archives betrachten:

```bash
tar tvfz fileName.tar
```

Einzelne Datei / einzelnes Verzeichnis aus Archiv entpacken:

```bash
tar xvfz fileName.tar /path/to/file/in/archive
```