# youtube-dl

## Schalter

- `--ignore-errors`  
Fehler ignorieren, kaputte Videos werden übersprüngen.
- `-x`  
Audio extrahieren
- `--audio-format "mp3"`  
Audio in mp3 umwandeln, ffmpeg wird benötigt.
- `--embed-thumbnail`  
Video-Thumbnail in die MP3 einbetten
- `--audio-quality 0`  
Bestmögliche Audio-Qualität

## Beispiele

Download einer Youtube-Playlist mit anschließender Konvertierung in MP3 mit `ffmpeg`.

```bash
youtube-dl -x --audioformat "mp3" --audio-quality 0 --embed-thumbnail --ignore-errors <playlistID>
```