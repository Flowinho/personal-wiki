# 🤲 Ubuntu Server

## 🔒 Standard SSH Port wechseln

Als Erstes muss sichergestellt werden dass die Firewall des Servers, in diesem Beispiel ufw) auch nach Änderung Ports diesen zulässt, da man sich sonst aus dem eigenen Server aussperrt.

Beispielhaft soll der Port 44344 eingerichtet werden.

```bash
sudo ufw allow 44344/tcp
```

Danach muss die Konfiguration des SSH-Daemons geändert werden, damit der SSH-Daemon ab sofort den gewünschten Port benutzt. Dazu muss die Datei `sshd_config` mit einem beliebigen Texteditor geöffnet werden.

```bash
sudo vim /etc/ssh/sshd_config
```

In dieser Datei wird der Port definiert, denn der Daemon nutzt. Standardmäßig ist Port 22 definiert. Innerhalb der Konfigurationsdatei wird vermutlich `# Port 22` stehen, da es sich um den Standard handelt. Um den Port zu ändern genügt es das `#` am Beginn der Zeile zu entfernen und `Port 22` durch beispielsweise `Port 44344` zu ersetzen. Danach die Datei speichern und den Editor verlassen.

Anschließend mit super-user Rechten den Service neu starten.

```bash
sudo systemctl restart sshd
```

Ausloggen und verifzieren dass Port 22 nicht mehr zur Verfügung steht. Ab sofort muss man sich durch Spezifikation des Ports anmelden, zB.

```bash
ssh user@host -p 44344
```
