---
description: Rezept für 4 Personen
---

# 🍠 Süßkartoffel-Cocos-Curry mit Kichererbsen

Dieses Süßkartoffel-Kokos-Curry ist würzig, wärmend, hält lange satt und schmeckt dabei auch noch unheimlich gut. Es ist absolut gelingsicher und bedarf nur wenigen Zubereitungsschritten. Das Curry kommt außerdem sehr gut als veganes Pendant zu Chili Con Carne auf Feiern an - nicht nur bei Vegetariern und Veganern. Zucchini & Süßkartoffel lassen sich auch mit Gemüse austauschen, das man sowieso zu Hause hat.

## 🍆 Zutaten

* 1 Dose Kokosmilch
* 1 Dose stückige Tomaten \(oder ca. 500 g frische Tomaten, s. Anmerkungen\)
* ca. 1 cm frischer Ingwer gerieben
* 1 Süßkartoffel
* 1 Zucchini
* 1 EL Chiliflocken \(mehr oder weniger, nach Geschmack\)
* 1/2 Glas/ Dose Kichererbsen \(ca. 120 g\)
* 1/2 EL Miso \(Alternative, s. Rezept-Anmerkungen\)
* 1 Bund Petersilie oder andere frische Kräuter
* Optional: Reis zum Servieren

## 🛒 Einkaufsliste

* 1 Dose Kokosmilch
* 1 Dose stückige Tomate
* 1 Ingwer / wahlweise gerieben
* 1 Süßkartoffel
* 1 Zucchini
* 1 Packung Chiliflocken
* 1 Glas Kichererbsen
* 1 Packung Miso
* 1 Bund Petersilie
* 1 Beutel Reis

## 👨‍🍳 Zubereitung

* Kokosmilch und Tomaten zusammen mit dem Ingwer in einem Topf auf mittlerer Stufe erhitzen.
* Süßkartoffel und Zucchini in mundgerechte Stücke schneiden. Gemüsewürfel und Chili in den Topf geben und das Ganze mit Deckel bei niedriger-mittlerer Stufe etwa 25 Minuten köcheln lassen.
* Das Miso mit 1 EL warmem Wasser in einer Schüssel verrühren. Die abgetropften Kichererbsen mit dem Miso vermengen und zu dem Curry geben. Weitere 5-10 Minuten bei niedrigster Stufe ziehen lassen.
* Mit Petersilie verfeinern und mit Reis, Brot oder einfach so direkt servieren.

## 📒 Notizen

* Die Dosentomaten können natürlich auch mit frischen Tomaten ersetzt werden. Allerdings machen die aus der Dose das Leben oft nicht nur so viel einfacher, sie haben meist auch einen intensiveren Geschmack. Das ist auch der Grund dafür, dass sie \(übrigens auch von Spitzenköchen\) vor allem für Soßen verwendet werden. Ich würde nur darauf achten, dass keine unnötigen Zusatzstoffe in der Dose sind. Die frischen Tomaten würde ich häuten und für die Intensität des Geschmacks auch noch einen Klecks Tomatenmark dazugeben.
* Die Kichererbsen können auch mit grünen Erbsen \(z.B. tiefgekühlt\) oder anderen Bohnen ersetzt werden.
* Miso lässt sich nur schwer ersetzten. Aber folgendes Rezept sollte Abhilfe schaffen: 1/2 EL Tahini \(Sesammus\), 1/2 TL Sojasauce/ Tamari, 1/2 TL Ahornsirup \(oder anderes flüssiges Süßungsmittel\)
* Das Curry lässt sich sehr gut einfrieren oder aber einige Tage im Kühlschrank aufbewahren. Ich koche oft eine größer Menge, da dies vieles erleichtert \(z.B. den Abwasch\).
* Da das Miso \(bzw. die Sojasauce, wenn verwendet\) schon sehr salzig ist, gebe ich kein extra Salz dazu. Je nachdem, was Du verwendest, kannst du das Curry zum Schluss noch etwas abschmecken.

