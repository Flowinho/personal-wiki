# 🐓 Hühnersuppe

![](../../.gitbook/assets/c4890c9e-d61d-44d2-929c-c3a87a7bce1a.jpeg)

## 🍆 Zutaten

* 500g-1000g Hühnerschlägel \(oder Suppenhuhn oder Hühnerunterschlägel, ggfs TK; Alternativ geht auch anderes Suppenfleisch, zB vom Rind oder Suppenknochen\)
* 1 Packung Suppengemüse aus der Gemüseabteilung
* Petersilie \(teilweise im Suppengemüse dabei\)
* 3 Lorbeerblätter
* ggf optional Kümmel
* optional 1 Paprika
* 1 große Zwiebel
* 1-2 Knoblauchzehen
* 2-3 Kartoffeln \(Drillinge oder vorwiegend festkochend\)
* Wirsing/Broccoli/etc \(zum generell die Suppe noch evtl verfeinern\)

## 👨🏻‍🍳 Zubereitung

* Hühnerschlägel, grob gewürfelte Zwiebel und Knoblauch mit Wasser überdenken, 3 Lorbeerblätter dazu, etwas Kümmel und Salz/Pfeffer/Paprikapulver und min. 1-1½h kochen.
* Währendessen das Gemüse schnibbeln. Nach 1-1½h die Schlegel rausnehmen, das Gemüse in die Suppe rein, kochen lassen.
* Währendessen das Fleisch vom Knochen trennen und zur Suppe wieder dazu tun.
* Das Gemüse muss auch nur so 20-30 Minuten drin kochen.
* Die Petersilie ganz zum Schluss dazumengen.
* Solltest du die Suppe abschmecken wollen, wegen dem Salz, warte damit noch nachdem die Hühnerschlägel min. 20 Minuten gekocht haben, wegen Salmonellen Gefahr.
* Und ansonsten kannst du generell die Suppe ja noch mit Wirsing/Broccoli/etc verfeinern

