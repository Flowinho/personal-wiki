# 👨‍👧‍👧 Kinderkuchen

## 🤓 Vorbereitung

* Backofen auf 200° Ober- / Unterhitze vorheizen.

## 🍆 Zutaten

* Irgendwelches Obst das in den Kuchen gesteckt wird, zB. Äpfel, Himbeeren, Erdbeeren, etc. Oder alles gleichzeitig.
* 250g Mehl
* 150g Butter
* 150g Zucker \(ideal: Kokosblütenzucker\)
* 4 Eier
* 2 TL Backpulver
* 1 Prise Salz

## 👨🏻‍🍳 Zubereitung

* Alles außer das Obst in eine Schüssel geben und mit einem Mixer umrühren bis der Teig schön dickflüssig geworden ist und die gröbsten Klumpen verschwunden sind.
* Teig in die gefettete Springform geben.
* Kinder die Teigschüssel auslöffeln lassen.
* Die Kinder / der Koch steck\*en das Obst in den Teig.
* Im Backofen auf 200° Ober- / Unterhitze ca 40min lang backen.
* 15min lang abkühlen lassen.
* Optional mit beliebigem Guss überziehen \(zB. Kinderschokolade\).

