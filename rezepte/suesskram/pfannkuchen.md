# 🥞 Pfannkuchen

## 🍆 Zutaten

Für ca 4-6 Pfannkuchen:

* 250g Mehl
* 250ml Milch
* 2 Eier \(getrennt\)
* 1 Pck. Vanille-Zucker
* Bütter / Öl

Für mehr Pfannkuchen einfach multiplizieren.

## 👨🏻‍🍳 Zubereitung

* Mehl, Milch, Eigelb und Zucker getrennt verrühren.
* Eiweiß schaumig schlagen, sobald es fest wird unter den Teig heben.
* Teig in einer Pfanne auf mittlerer Hitze zu Pfannkuchen ausbacken.

