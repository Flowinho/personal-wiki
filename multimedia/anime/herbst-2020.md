# 🍂 Herbst 2020

## Übersicht 📝

Die Bewertung erfolgt in 1 - 7 Pandas 🐼🐼🐼🐼🐼🐼🐼.

| Anime | Folgen gesehen | Bewertung |  |
| :--- | :---: | :---: | :--- |
| Tensei shitara Slime Datta Ken OVA | 1 | 🐼🐼 |  |
| Kuma Kuma Kuma Bear | 0 | Unbewertet 🐼 |  |
| Hypnosis Mic - Division Rap Battle - Rhyme Anima | 0 | Unbewertet 🐼 |  |
| Senyoku no Sigrdrifa | 0 | Unbewertet 🐼 |  |
| Strike Witches - Road to Berlin | 3 | 🐼  |  |
| Hanyou no Yashahime | 0 | Unbewertet 🐼 |  |
| Adachi to Shimamura | 0 | Unbewertet 🐼 |  |
| Yuukoku no Moriarty | 0 | Unbewertet 🐼 |  |
| Mahouka Koukou no Rettousei | 0 | Unbewertet 🐼 |  |
| Noblesse | 0 | Unbewertet 🐼 |  |
| Kamisama ni Natta Hi | 0 | Unbewertet 🐼 |  |
| Akudama Drive | 0 | Unbewertet 🐼 |  |

## Kommentare 🤓

Alle Kommentare sind - sofern möglich - spoilerfrei.

### Tensei shitara Slime Datta Ken OVA

* Bereits Teil des offiziellen Releases von [Crunchyroll](https://crunchyroll.com).
* Recht langweilige Zurschaustellung nackter Haut.

### Strike Switches - Road to Berlin

* Teilweise lächerlicher Fan-Service.
* Keine nennenswerte Erweiterung der Story, die Protagonistinnen werden zusammengerufen, viel mehr passiert nicht.
* Neuerungen auf Neuroi-Seite eher enttäuschend.
* Rolle des "Schwächlings" von Mijafuyi weg versetzt zu neuer Figur, die auf langweilie Art und Weise der Mijafuyi aus Strike Witches 2 entspricht.
* Dropped nach Episode 3.

## Nützliches 🔧

{% page-ref page="../../glossar.md" %}

{% page-ref page="anime-englische-titel.md" %}