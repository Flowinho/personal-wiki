# 💬 Kontakt aufnehmen

## 📱Messenger

{% tabs %}
{% tab title="🌍 Öffentlich" %}
* [Threema-ID: ](https://threema.ch/de)ZXAWC484 - Lobst.er
* [Wire-ID: ](https://wire.com/de/)@L3afLight
* [IRC:](https://freenode.net/) flowinho @ irc.freenode.net, _Bouncer ist aktiviert._
{% endtab %}

{% tab title="👨‍👧‍👧 Engerer Kreis" %}
* [Signal](https://signal.org/de/): &lt;handynummer&gt;
* [iMessage:](https://support.apple.com/de-de/HT207006) &lt;appleID&gt;
{% endtab %}
{% endtabs %}

## 📧 Email

{% tabs %}
{% tab title="🌍 Öffentlich" %}
**Adresse:** contact@flowinho.com

**PGP Fingerabdruck:** 6861 7A53 E0FE 72A3 56A8 CBA1 FE20 C095 A363 330A

{% file src="../.gitbook/assets/flowinho-a363330a-public.asc" caption="PGP Public Key" %}
{% endtab %}

{% tab title="👨‍👧‍👧 Engerer Kreis" %}
Wird hier nicht eingetragen. 
{% endtab %}
{% endtabs %}

## ⌨️ Chat

{% tabs %}
{% tab title="🌍 Öffentlich" %}
* [IRC:](https://freenode.net/) flowinho @ irc.freenode.net, _Bouncer ist aktiviert._
* [Slack: ](https://slack.com/intl/en-de/)flowinho @ audiodump.slack.com -- Ein Einladungslink findet sich [hier](https://audiodump.de/slack).
{% endtab %}
{% endtabs %}



