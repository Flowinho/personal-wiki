# 🤡 Soziale Medien

{% hint style="info" %}
Um meinen digitalen Aktivitäten und philosophischen Ergüssen zu folgen, reicht es im Allgemeinen gelegentlich auf [https://feed.fschuttkowski.xyz/](https://feed.fschuttkowski.xyz/) zu schauen.
{% endhint %}

## 🏃🏻‍♂️Aktiv genutzt

Soziale Medien auf denen ich dir antworte.

{% embed url="https://chaos.social/@flowinho" %}

{% embed url="https://flowinho.micro.blog/" %}

## 🧊 Mirrors

Soziale Medien auf denen Inhalte aus anderen Netzwerken gespiegelt werden. Es kann sein dass ich hier sehr verzögert antworte - wenn überhaupt.

{% embed url="https://twitter.com/flowinho" %}



