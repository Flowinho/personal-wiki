---
title: Open Source Brain
description: >-
  Ein persönliches Archiv meiner kleinen Welt und meiner Erfahrungen. Der
  Versuch eines technologischen Erbes.
---

# 🧠 Open Source Brain

{% hint style="info" %}
Dieses Wiki wird am Besten über die Suchfunktion genutzt.
{% endhint %}

## Willkommen 🥳

Diese Wiki ist das öffentliche Archiv einiger meiner Erfahrungen, der Informationen die ich im Laufe der Zeit sammle und der Daten die ich mit der Welt teilen möchte. Frei nach [Ryan Nicodemus](https://www.theminimalists.com/about/):

> The best kind of person - is a kind person.

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/Flowinho/personal-wiki)

Dieses Projekt wird unter anderem mit Hilfe der Online - IDE [GitPod.io](https://gitpod.io) bearbeitet.

## Rechtliches

### Impressum 👩🏻‍💼

Inhaltlich für den Inhalt dieser Seite verantwortlich bin Ich, Florian Schuttkowski. Diese Seiten geben zu keinem Zeitpunkt die Meinung meiner Arbeitgeber oder anderer Dritter wieder. Für weitere Informationen werfen Sie bitte einen Blick in das [Impressum](./) meiner persönlichen Website.

### Datenschutz 👮🏻‍♂️

Diese Webseite wird von [GitBook](https://gitbook.com) gehostet, die Inhalte von [GitHub](https://github.com/flowinho/open-source-brain/tree/baad9f245786f8f92f0eb0023e585a5524cd17c7/synchronisiert/README.md) und Ressourcen von [meiner persönlichen Website](https://fschuttkowski.xyz) geladen. Deswegen verweise ich Sie folgend auf drei Datenschutzerklärungen:

* [GitBook Datenschutzerklärung](https://policies.gitbook.com/privacy)
* [GitHub Datenschutzerklärung](https://docs.github.com/en/free-pro-team@latest/github/site-policy/github-privacy-statement)
* [Persönliche Datenschutzerklärung](https://fschuttkowski.xyz/data-policy.html)

